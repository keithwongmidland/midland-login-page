import React, { useContext } from 'react';
import styled from 'styled-components';
import { TranslationContext } from '@midlandddd/midland-translation';
import { Container, Row, Col, Hidden } from '@midlandddd/midland-component/core';

import Banner from './../../../../assets/images/zabank/banner.png'
import QR from './../../../../assets/images/zabank/qr.png'
import App from './../../../../assets/images/zabank/app.png'

const StyledIPrize1 = styled.div`
  padding: 40px 0 0;
`;

const StyledTitle = styled.h1``;

const StyledPreTitle = styled.h2``;


const StyledSubtitle = styled.h6``;

const StyledSubtitleMain = styled.span`
  font-weight:500;
`;

const StyledDescription = styled.p``;

const StyledImg = styled.img`
  width:100%;
`;

const Prize1 = () => {
  const { t } = useContext(TranslationContext);

  return (
    <StyledIPrize1>
      <Container>
        <Row>
          <Col>
            <StyledPreTitle>{t('prize1:headline')}</StyledPreTitle>
            <StyledImg src={Banner} alt={t('prize1:headline')} />
            <StyledDescription>{t('prize1:description')}</StyledDescription>
          </Col>
        </Row>
        <Row>
          <Col col={12}>
            <StyledSubtitle>
              <StyledSubtitleMain>{t('prize1:step1:step')}</StyledSubtitleMain>
              <span dangerouslySetInnerHTML={{ __html: t('prize1:step1:todo') }} />
            </StyledSubtitle>
          </Col>
          <Col col={12} sm={4}>
            <div>
              <StyledImg src={QR} alt={t('prize1:step1:step')} />
            </div>
          </Col>
        </Row>
        <Row>
          <Col col={12}>
            <StyledSubtitle>
              <StyledSubtitleMain>{t('prize1:step2:step')}</StyledSubtitleMain>
              <span>{t('prize1:step2:todo')}</span>
            </StyledSubtitle>
          </Col>
          <Col col={12} sm={4}>
            <StyledImg src={App} alt={t('prize1:step2:step')} />
          </Col>
        </Row>
        <Row>
          <Col>
            <StyledSubtitle>
              <StyledSubtitleMain>{t('prize1:step3:step')}</StyledSubtitleMain>
              <span>{t('prize1:step3:todo')}</span>
            </StyledSubtitle>
          </Col>
        </Row>
        <Row>
          <Col>
            <StyledDescription dangerouslySetInnerHTML={{ __html: t('prize1:terms') }} />
          </Col>
        </Row>
      </Container>

    </StyledIPrize1>
  )
};

export default Prize1;