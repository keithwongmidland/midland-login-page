import React, { useContext } from 'react';
import styled from 'styled-components';
import { TranslationContext } from '@midlandddd/midland-translation';
import { Container, Row, Col, Hidden } from '@midlandddd/midland-component/core';

import Monoploy from './../../../../assets/images/zabank/monopoly.png'


const StyledIPrize2 = styled.div`
  padding: 40px 0 0;

`;

const StyledPreTitle = styled.h2``;


const StyledDescription = styled.p``;

const StyledImg = styled.img`
  width:100%;
`;

const Prize2 = () => {
  const { t } = useContext(TranslationContext);

  return (
    <StyledIPrize2>
      <Container>
        <Row>
          <Col col={12}>
            <StyledPreTitle>{t('prize2:headline')}</StyledPreTitle>
          </Col>
          <Col col={12}>
            <StyledImg src={Monoploy} alt={t('prize2:headline')} />
          </Col>
          <Col col={12}>
            <StyledDescription dangerouslySetInnerHTML={{__html:t('prize2:description')}}/>
            <StyledDescription dangerouslySetInnerHTML={{__html:t('prize2:terms')}}/>
          </Col>
        </Row>
      </Container>
    </StyledIPrize2>
  )
};

export default Prize2;