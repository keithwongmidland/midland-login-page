import React, { useContext } from 'react';
import styled from 'styled-components';
import { TranslationContext } from '@midlandddd/midland-translation';
import { Container, Row, Col, Hidden } from '@midlandddd/midland-component/core';

const StyledIntro = styled.div``;

const StyledTitle = styled.h1``;
const StyledDescription = styled.p``;

const Intro = () => {
  const { t } = useContext(TranslationContext);

  return (
    <StyledIntro>
      <Container>
        <Row>
          <Col>
            <StyledTitle>{t('intro')}</StyledTitle>
            <StyledDescription>{t('description1')}</StyledDescription>
            <StyledDescription>{t('description2')}</StyledDescription>
          </Col>
        </Row>
      </Container>

    </StyledIntro>
  )
};

export default Intro;