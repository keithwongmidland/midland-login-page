import en from './en';
import zhCN from './zh-cn';
import zhHK from './zh-hk';

export default {
  en,
  'zh-cn': zhCN,
  'zh-hk': zhHK,
};
