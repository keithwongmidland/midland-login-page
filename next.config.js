

module.exports = {
  webpack(config, { isServer }) {
    const assetPrefix = (process.env.NODE_ENV === 'production' && process.env.ASSET_PREFIX) ? (process.env.ASSET_PREFIX) : '';

    config.module.rules.push({
      test: /\.(jpe?g|JPE?G|png|gif|svg|ico)$/,
      use: [
        {
          loader: 'url-loader',
          options: {
            limit: 1,
            fallback: 'file-loader',
            publicPath: `${assetPrefix}/_next/static/images/`,
            outputPath: 'static/images',
            name: '[name].[ext]',
          },
        },
      ],
    });
    config.output.chunkFilename = isServer ? '[name].js' : 'static/chunks/[name].js';
    config.output.filename = '[name]';
    return config;
  },
}