
import React, { useContext } from 'react';
import styled from 'styled-components';
import { TranslationContext } from '@midlandddd/midland-translation';

import Intro from '../../components/Section/zabank/intro';
import Prize1 from '../../components/Section/zabank/prize1';
import Prize2 from '../../components/Section/zabank/prize2';

const StyledContentPage = styled.div``;

const ContentPage = () => {
  const { t } = useContext(TranslationContext);

  return (
    <StyledContentPage>
      <Intro />
      <Prize1 />
      <Prize2 />
    </StyledContentPage>
  )
}

export default ContentPage