
import React from 'react';
import App from 'next/app';
import { TranslationContextProvider } from '@midlandddd/midland-translation';
import { MidlandThemeProvider } from '@midlandddd/midland-component/core';

import translation from '../assets/locales';

const MyApp = ({ Component, pageProps, router }) => {
  const { lang: langCode } = router.query;

  console.log(translation[langCode])

  return (
    <>
      <MidlandThemeProvider langCode={langCode}>
        <TranslationContextProvider translation={translation[langCode]}>
          <Component {...pageProps} />
        </TranslationContextProvider>
      </MidlandThemeProvider>
    </>
  );
};

MyApp.getInitialProps = async (appContext) => {
  // calls page's `getInitialProps` and fills `appProps.pageProps`
  const { ctx = {} } = appContext;
  const { req = {} } = ctx;
  const { headerData = {} } = req;
  const appProps = await App.getInitialProps(appContext);
  return { ...appProps, headerData, };
};


export default MyApp;